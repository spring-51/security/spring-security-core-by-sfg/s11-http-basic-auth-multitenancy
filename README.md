# Http Basic User Role Authorities - Java Based Configuration
# refer SecurityConfig, UserLoader and h2 console for table schema 

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

## L65 - Intro 
```
1. Similer to role in authority also we get 403.
2. Authorities give more granular level AUTHORIZATION as compared to roles.
  - Roles are broad in scope

```

## L66 - Overview db refactoring
```
1. Authorities give more granular level AUTHORIZATION as compared to roles.
  - Roles are broad in scope.
2. With authorities we can control access to CRUD operation AUTHORIZATION.
3. Refactor db as 
  Users --MANYTOMANY-- Roles --MANYTOMANY-- Authorities
4. refer ./L66-RefactorRoles.pdf
```

## L67 - JPA entity refactoring - Part 1
```
1. Added CustomRole
   added CustomRoleRepository
   Refctored CustomUser
   Refactored CustomAuthority
   This refactoring to done to achieve
   Users --MANYTOMANY-- Roles --MANYTOMANY-- Authorities
```

## L68 - JPA entity refactoring -  Part 2
```
1. Refctored  UserDataLoader because we changed entities and relationship
```

## Notes
```
1. I have added two branch
  - one where we used authorities in Java config (branchName - final-using-java-configuration-in-SecurityConfig-class)
  - second where we used authorities in method (branchName - final-using-method-security)
     -- refer SecurityConfig -> configure(HttpSecurity)
     -- refer AdminController   
     -- refer CustomerController
     -- refer UserController
     -- JUnit tests
       -- refer AdminControllerTests
       -- refer CustomerControllerTests
       -- refer UserControllerTests
```

## L70 - Custom Authorization annotation
```
1. if we are using a common aithority in more than one method
then if we chnage the authority string value we need to change all the place
  - refer UserController -> getAPI(). getAPI2(), getAPI3()

2. To overcome above problem we can create annotation for common authority  
  - Steps
    -- step1 - create new annotation
    -- step2 - add retention policy as RetentionPolicy.RUNTIME
    -- step3 - add @PreAuthorize("hasAnyAuthority('some.common.authority')")
      --- refer - UserGetAuthority
    -- step4 - replace  @PreAuthorize("hasAnyAuthority('some.common.authority')") with new annotation.
    --- refer UserController -> getAPI(). getAPI2(), getAPI3()
    
    -- Junit tests
      --- UserControllerTests
```