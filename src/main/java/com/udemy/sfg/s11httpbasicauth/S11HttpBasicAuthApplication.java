package com.udemy.sfg.s11httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S11HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S11HttpBasicAuthApplication.class, args);
    }
}
