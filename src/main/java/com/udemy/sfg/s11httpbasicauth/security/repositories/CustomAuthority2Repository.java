package com.udemy.sfg.s11httpbasicauth.security.repositories;

import com.udemy.sfg.s11httpbasicauth.security.dtos.CustomAuthority2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomAuthority2Repository extends JpaRepository<CustomAuthority2,Integer> {
}
