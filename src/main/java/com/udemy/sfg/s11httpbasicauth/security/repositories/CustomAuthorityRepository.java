package com.udemy.sfg.s11httpbasicauth.security.repositories;

import com.udemy.sfg.s11httpbasicauth.security.dtos.CustomAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomAuthorityRepository extends JpaRepository<CustomAuthority, Integer> {
}
