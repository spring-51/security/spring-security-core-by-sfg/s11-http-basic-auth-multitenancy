package com.udemy.sfg.s11httpbasicauth.security.repositories;

import com.udemy.sfg.s11httpbasicauth.security.dtos.CustomRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomRoleRepository extends JpaRepository<CustomRole, Integer> {
}
